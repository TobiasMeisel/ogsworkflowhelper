from ogs6py import ogs
import pandas as pd
import argparse
import fileinput


def row_to_xml_gn(medium_template_filename):
    def row_to_xml(row):
        layer_id = row.name
        medium_filename = "OGSmedium_{0}.xml".format(layer_id)
        medium_xml = ogs.OGS(INPUT_FILE=medium_template_filename, PROJECT_FILE=medium_filename)
        for i, col_name in enumerate(row.index):
            xpath = "./properties/property[name='{0}']/value".format(col_name)
            medium_xml.replace_text(row.iloc[i], xpath=xpath)
        return medium_xml

    return row_to_xml


def csv2pandas(model_name, csv_parameter_file, process):
    """ Filters specific model from csv parameter file
    model_name: 'Ton-Nord' or 'Ton-Sued'
    csv_parameter_file: file_name_path to csv file
    process: 'simH_1' or 'simHM_1'
    """
    # Process

    all_process_variables = {
        # H process
        'simH_1': {'hydraulic_permeability_[m^2]_mean': 'permeability', 'Porosity_[-]_mean': 'porosity'},
        # HM process
        'simHM_1': {'hydraulic_permeability_[m^2]_mean': 'permeability', 'Porosity_[-]_mean': 'porosity'}}

    process_variables = all_process_variables[process]

    # filter and rename
    parameter_table = pd.read_csv(csv_parameter_file, delimiter=';')
    parameter_table = parameter_table.loc[parameter_table['model_id'] == model_name]
    index = ['layer_id', 'model_unit']
    parameter_table = parameter_table.rename(process_variables, axis='columns')
    parameter_table = parameter_table[index + list(process_variables.values())]
    parameter_table['row_num'] = parameter_table.reset_index().index
    parameter_table = parameter_table.set_index('row_num')
    return parameter_table


def pandas2prj(parameter_table, process, prj_template_folder):
    """ Creates from table OGS prj file with references to media xmls
    parameter_table: pandas table for a single model
    process: Process e.g. 'simH_1'
    prj_template_folder: file_name_path to folder containing template prj file and template media xml file
    """
    template_prj = prj_template_folder + "/{0}.prj".format(process)
    template_media = prj_template_folder + "/OGStemplate_medium.xml"
    # Output
    main_filename = "{0}.prj".format(process)
    # create media xml and write
    row_to_xml = row_to_xml_gn(template_media)
    media_xml = parameter_table.apply(row_to_xml, axis=1)
    _ = [medium_xml.write_input() for medium_xml in media_xml]

    # The <medium> tags were needed for ogs6py but are removed now,
    # they will be reinserted as numbered <medium id ...>  in main.prj.
    # This approach was chosen, as OGS allows only one file inclusion per XML-tag
    for medium in media_xml:
        for line in fileinput.input(medium.prjfile, inplace=True):
            if 'medium' in line:
                continue
            print(line, end='')

    # extent prj file with references to media
    main_prj = ogs.OGS(INPUT_FILE=template_prj, PROJECT_FILE=main_filename)
    for k, v in media_xml.items():
        medium_xml = media_xml[k]
        include_file = "<include file=\"{0}\"/>"
        main_prj.add_entry(parent_xpath="./media", tag="medium", text=include_file.format(medium_xml.prjfile),
                           attrib="id", attrib_value=str(k))
    main_prj.write_input()

    # ogs6py codes "<" as "&lt;" and ">" as  "&gt;",
    # although XML readers should accept this, we replace it for sake of beauty
    main_filedata = open(main_filename).read()
    main_filedata = main_filedata.replace('&lt;', '<')
    main_filedata = main_filedata.replace('&gt;', '>')
    main_file = open(main_filename, 'w')
    main_file.write(main_filedata)
    return main_filename


if __name__ == '__main__':
    """ Generates prj and media xml files from csv parameter table
    
    Take all rows with specified model from csv_parameter_file. 
    Create for all rows a material xml bases on a material template file found on prj_template_folder. 
    Reference all material files in process template found on prj_template folder and named process.prj"
    
    model_name: e.g.'Ton-Nord'
    process: e.g. 'simH_1'
    prj_template_folder: file_name_path to folder containing template prj file and template media xml file
    csv_parameter_file: file_name_path to csv file
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("model_name", help="e.g. # Ton-Nord, Ton-Sued",
                        type=str)
    parser.add_argument("csv_parameter_file", help="full path to (csv) file that contains material parameter")
    parser.add_argument("process", help="e.g. simH_1")
    parser.add_argument("prj_template_folder", help="full path to folder of process template (prj and medium xml)")

    args = parser.parse_args()
    name_modelid = {'Ton-Nord': 1, 'Ton-Sued': 2}
    model_id = name_modelid[args.model_name]
    param_table = csv2pandas(model_name=model_id, process=args.process, csv_parameter_file=args.csv_parameter_file)
    pandas2prj(param_table, process=args.process, prj_template_folder=args.prj_template_folder)
